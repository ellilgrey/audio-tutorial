﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowObject : MonoBehaviour
{
    //public variables
    public GameObject projectile;
    public AudioClip shootSound;
    public float throwSpeed = 2000f;

    //private variables
    private AudioSource source;
    private float volLowRange = .5f;
    private float volHighRange = 1f;
    private float vol;

    Rigidbody body;
    // Start is called before the first frame update
    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            //set the volume
            vol = Random.Range(volLowRange, volHighRange);
            //play the audio
            source.PlayOneShot(shootSound, vol);
            //throw the ball
            GameObject throwThis = Instantiate (projectile, transform.position, transform.rotation) as GameObject;
            body = throwThis.GetComponent<Rigidbody>();
            body.AddRelativeForce(new Vector3(0, 0, throwSpeed));
        }
    }
}
