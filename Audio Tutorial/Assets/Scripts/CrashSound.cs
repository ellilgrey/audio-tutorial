﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrashSound : MonoBehaviour
{
    //public variables
    public AudioClip crashSoft;
    public AudioClip crashHard;

    //private variables
    private AudioSource source;
    private float lowPitchRange = .75f;
    private float highPitchRange = 1.25f;
    private float velToVol = .2f;
    private float velocityClipSplit = 10f;
    private float hitVol;
    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        //set the pitch of the source randomly
        source.pitch = Random.Range(lowPitchRange, highPitchRange);

        //set the volume of the crash relative to velocity
        hitVol = collision.relativeVelocity.magnitude * velToVol;

        //if velocity is greater than 10 play the loud sound, if not play the soft sound
        if(collision.relativeVelocity.magnitude < velocityClipSplit)
        {
            source.PlayOneShot(crashSoft, hitVol);
        }else
        {
            source.PlayOneShot(crashHard, hitVol);
        }
    }
}
